//
//  URL+Fakes.swift
//  MoviesTests
//
//  Created by Rodrigo Gonzalez on 09/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import Foundation

class URLSessionDataTaskFake: URLSessionDataTask {
    
    let urlResponse: URLResponse?
    let data: Data?
    let theError: Error?
    let completionHandler: (Data?, URLResponse?, Error?) -> Void
    
    init(data: Data?, response: URLResponse?, error: Error?, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        self.data = data
        self.urlResponse = response
        self.theError = error
        self.completionHandler = completionHandler
    }
    
    override func resume() {
        completionHandler(data, urlResponse, theError)
    }
}

class URLSessionFake: URLSession {
    var responseData: Data?
    var urlResponse: URLResponse?
    var error: Error?
    
    override init() {
        
    }
    
    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        
        return URLSessionDataTaskFake(data: responseData, response: urlResponse, error: error, completionHandler: completionHandler)
    }
    
    override func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        let request = URLRequest(url: url)
        return dataTask(with: request, completionHandler: completionHandler)
    }
}

