//
//  RequestGeneration+Tests.swift
//  MoviesTests
//
//  Created by Rodrigo Gonzalez on 09/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import XCTest
@testable import Movies

class RequestGeneration_Tests: XCTestCase {

    func testInitWithValidParameters_ShouldReturnURL() throws {
        // Given
        let endpoint = "https://www.validURL.com/endpoint"
        let key = "theApiKey"
        
        let requestGeneration = try XCTUnwrap(RequestGeneration(endpoint: endpoint, key: key))
        
        // When
        let result = requestGeneration.latestMovies()
        
        // Then
        XCTAssertNotNil(result)
    }
    
    func testInitWithInvalidParameters_ShouldReturnNil() {
        // Given
        let endpoint = "invalid%^&*Endpoint"
        let key = "theApiKey"
        
        // When
        let requestGeneration = RequestGeneration(endpoint: endpoint, key: key)
        
        // Then
        XCTAssertNil(requestGeneration)
    }

}
