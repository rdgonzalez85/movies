//
//  RequestGeneration+Fakes.swift
//  MoviesTests
//
//  Created by Rodrigo Gonzalez on 09/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import Foundation
@testable import Movies

class RequestGenerationFake: RequestGenerator {
    
    var _latestMovies: URL? = URL(fileURLWithPath: "")
    
    func latestMovies() -> URL? {
        return _latestMovies
    }
    
}
