//
//  GetMoviesPresenter+Tests.swift
//  MoviesTests
//
//  Created by Rodrigo Gonzalez on 10/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import XCTest
@testable import Movies

class GetMoviesPresenter_Tests: XCTestCase {

    func testGetMovies_ShouldUpdateValues() {
        // Given
        let fakeMovie = Movie.fake()
        let dataSource = MoviesDataSourceFake()
        dataSource.movies = [fakeMovie]
        
        let presenter = GetMoviesPresenter(dataSource: dataSource)
        
        XCTAssertEqual(presenter.numberOfSections(), 0)
        XCTAssertEqual(presenter.numberOfRowsInSection(section: 0), 0)
        XCTAssertNil(presenter.itemForIndexPath(indexPath: IndexPath(row: 0, section: 0)))
        
        
        let testExpectation = expectation(description: "testGetMovies_ShouldUpdateValues")
        let expectedMovie = UIMovie(title: fakeMovie.title, description: fakeMovie.overview)
        
        // When
        presenter.getMovies {
        
            // Then
            XCTAssertEqual(presenter.numberOfSections(), 1)
            XCTAssertEqual(presenter.numberOfRowsInSection(section: 0), 1)
            XCTAssertEqual(presenter.itemForIndexPath(indexPath: IndexPath(row: 0, section: 0)), expectedMovie)
            testExpectation.fulfill()
        }
        
        wait(for: [testExpectation], timeout: 0.5)
    }
}
