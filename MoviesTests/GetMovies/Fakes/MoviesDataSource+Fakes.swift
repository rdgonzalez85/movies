//
//  MoviesDataSource+Fakes.swift
//  MoviesTests
//
//  Created by Rodrigo Gonzalez on 10/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import Foundation
@testable import Movies

class MoviesDataSourceFake: MoviesDataSource {
    
    var movies: [Movie]?
    var error: Error?
    
    func getMovies(completion: @escaping MoviesResponse) {
        if let movies = movies {
            completion(.success(movies))
            return
        }
        
        if let error = error {
            completion(.failure(error))
            return
        }
    }
}
