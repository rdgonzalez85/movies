//
//  ProviderResponse+Fakes.swift
//  MoviesTests
//
//  Created by Rodrigo Gonzalez on 09/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import Foundation
@testable import Movies

extension Movie {
    static func fake() -> Movie {
        return Movie(popularity: 1, voteCount: 10, video: true, posterPath: "posterPath", id: 1, adult: true, backdropPath: "backdropPath", originalLanguage: "originalLanguage", originalTitle: "originalTitle", genreIDS: [1], title: "title", voteAverage: 1.0, overview: "overview", releaseDate: "2001-01-01")
    }
}

extension ProviderResponse {
    static func fake() -> ProviderResponse {
        return ProviderResponse(page: 1, totalResults: 1, totalPages: 1, results: [Movie.fake()])
    }
    
    func jsonData() -> Data? {
        let encoder = JSONEncoder()
        return try? encoder.encode(self)
    }
}
