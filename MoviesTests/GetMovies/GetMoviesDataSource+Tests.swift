//
//  GetMoviesDataSource+Tests.swift
//  MoviesTests
//
//  Created by Rodrigo Gonzalez on 09/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import XCTest
@testable import Movies

class GetMoviesDataSource_Tests: XCTestCase {

    func testGetMovies_ShouldReturnValue() {
        // Given
        let session = URLSessionFake()
        let provider = ProviderResponse.fake()
        
        session.responseData = provider.jsonData()
        let requestGenerator = RequestGenerationFake()
        let dataSource = GetMoviesDataSource(requestGenerator: requestGenerator, session: session)
        
        let getMoviesExpectation = expectation(description: "testGetMovies_ShouldReturnValue")
     
        // When
        dataSource.getMovies { (result) in
            switch result {
            case .success(let movies):
                guard let movie = movies.first, let providerMovie = provider.results.first else {
                    XCTFail()
                    return
                }

                // Then
                XCTAssertEqual(providerMovie, movie)
                getMoviesExpectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }

        waitForExpectations(timeout: 0.5, handler: nil)
    }
    
    func testGetMoviesWithError_ShouldReturnError() {
        // Given
        let session = URLSessionFake()
        
        session.error = CustomError.message(message: "test error")
        let requestGenerator = RequestGenerationFake()
        let dataSource = GetMoviesDataSource(requestGenerator: requestGenerator, session: session)
        
        let getMoviesExpectation = expectation(description: "testGetMoviesWithError_ShouldReturnError")
     
        // When
        dataSource.getMovies { (result) in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(error.localizedDescription, session.error?.localizedDescription)
                getMoviesExpectation.fulfill()
            }
        }

        waitForExpectations(timeout: 0.5, handler: nil)
    }
    
    func testGetMoviesWithoutURL_ShouldReturnError() {
        // Given
        let session = URLSessionFake()
        let requestGenerator = RequestGenerationFake()
        requestGenerator._latestMovies = nil
        
        let dataSource = GetMoviesDataSource(requestGenerator: requestGenerator, session: session)
        
        let getMoviesExpectation = expectation(description: "testGetMoviesWithoutURL_ShouldReturnError")
     
        // When
        dataSource.getMovies { (result) in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertNotNil(error)
                getMoviesExpectation.fulfill()
            }
        }

        waitForExpectations(timeout: 0.5, handler: nil)
    }
}
