//
//  ViewControllerLoading+Fakes.swift
//  MoviesTests
//
//  Created by Rodrigo Gonzalez on 10/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import UIKit
@testable import Movies

class ViewControllerLoadingFake: ViewControllerLoading {
    var viewController: UIViewController?
    
    func viewController(storyboardName: String, viewControllerIdentifier: String) -> UIViewController {
        return viewController ?? UIViewController()
    }
}

