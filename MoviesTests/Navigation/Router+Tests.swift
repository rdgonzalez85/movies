//
//  Router+Tests.swift
//  MoviesTests
//
//  Created by Rodrigo Gonzalez on 10/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import XCTest
@testable import Movies

class Router_Tests: XCTestCase {

    func testGetMoviesViewController_ShouldReturnExpectedViewController() throws {
        // Given
        let loader = ViewControllerLoadingFake()
        loader.viewController = GetMoviesTableViewController()
        let requestGeneration = RequestGenerationFake()
        let router = MoviesRouter(requestGeneration: requestGeneration, loader: loader)
        
        // When
        let viewController = router.getMoviesViewController()
        
        // Then
        let moviesViewController = try XCTUnwrap(viewController as? GetMoviesTableViewController)
        XCTAssertEqual(moviesViewController, loader.viewController)
        XCTAssertNotNil(moviesViewController.presenter)
    }
}
