//
//  RequestGeneration.swift
//  Movies
//
//  Created by Rodrigo Gonzalez on 09/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import Foundation

protocol RequestGenerator {
     func latestMovies() -> URL?
}

class RequestGeneration: RequestGenerator {
    private let urlComponents: URLComponents
    private let apiKey = "api_key"
    
    convenience init?(endpoint: String, key: String) {
        if let url = URL(string: endpoint) {
            self.init(baseURL: url, key: key)
        } else {
            return nil
        }
    }
    
    init?(baseURL: URL, key: String) {
        if var components = URLComponents(url: baseURL, resolvingAgainstBaseURL: false) {
            let queryItems = URLQueryItem(name: apiKey, value: key)
            components.queryItems = [queryItems]
            self.urlComponents = components
        } else {
            return nil
        }
        
    }
    
    func latestMovies() -> URL? {
        return urlComponents.url
    }

}
