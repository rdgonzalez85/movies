//
//  MoviesRouter.swift
//  Movies
//
//  Created by Rodrigo Gonzalez on 09/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import UIKit

protocol Router {
    func getMoviesViewController() -> UIViewController
}

class MoviesRouter: Router {
    
    private let storyboardName: String
    private let loader: ViewControllerLoading
    private let requestGeneration: RequestGenerator
    
    init(requestGeneration: RequestGenerator, storyboardName: String = "Main", loader: ViewControllerLoading = ViewControllerLoader()) {
        self.storyboardName = storyboardName
        self.loader = loader
        self.requestGeneration = requestGeneration
    }

    func getMoviesViewController() -> UIViewController {
               
        let viewController = loader.viewController(storyboardName: storyboardName, viewControllerIdentifier: GetMoviesTableViewController.identifier)
        
        if let viewController = viewController as? GetMoviesTableViewController  {
            let dataSource = GetMoviesDataSource(requestGenerator: requestGeneration)
            let presenter = GetMoviesPresenter(dataSource: dataSource)
           
           viewController.presenter = presenter
        }
        
        return viewController
    }
}


protocol StoryboardInstantiateable {
    static var identifier: String { get }
}

extension StoryboardInstantiateable {
    static var identifier: String {
        return String(describing: self)
    }
}

protocol ViewControllerLoading {
    func viewController(storyboardName: String, viewControllerIdentifier: String) -> UIViewController
}

extension ViewControllerLoading {
    func viewController(storyboardName: String, viewControllerIdentifier: String) -> UIViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerIdentifier)
        
        return controller
    }
}

class ViewControllerLoader: ViewControllerLoading {
    // empty class, will use the methods from the protocol
}
