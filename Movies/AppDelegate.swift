//
//  AppDelegate.swift
//  Movies
//
//  Created by Rodrigo Gonzalez on 09/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if let router = makeRouter() {
            let mainView = router.getMoviesViewController()
            let mainNavigationVC = UINavigationController(rootViewController: mainView)
            window?.rootViewController = mainNavigationVC
            window?.makeKeyAndVisible()
        } else {
            print("error: wrong request generator - check URL or API_KEY")
        }
        return true
    }
    
    private func makeRouter() -> MoviesRouter? {
        guard let requestGeneration = RequestGeneration(endpoint: "https://api.themoviedb.org/3/movie/popular", key: "#ENTER-YOUR-API-KEY") else {
            return nil
        }
        return MoviesRouter(requestGeneration: requestGeneration)
    }
    
}

