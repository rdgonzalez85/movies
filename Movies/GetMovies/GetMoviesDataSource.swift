//
//  GetMoviesDataSource.swift
//  Movies
//
//  Created by Rodrigo Gonzalez on 09/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import Foundation

typealias MoviesResponse = (Result<[Movie],Error>) -> ()

protocol MoviesDataSource {
    func getMovies(completion: @escaping MoviesResponse)
}

class GetMoviesDataSource: MoviesDataSource, NetworkDataSource {

    private let session: URLSession
    private let requestGenerator: RequestGenerator
    
    init(requestGenerator: RequestGenerator, session: URLSession = .shared) {
        self.session = session
        self.requestGenerator = requestGenerator
    }
    
    func getMovies(completion: @escaping MoviesResponse) {
        guard let url = requestGenerator.latestMovies() else {
            completion(.failure(CustomError.message(message: "missing url")))
            return
        }
        
        get(url: url, session: session) { [weak self] responseData, requestError in
            guard let self = self else {
                completion(.failure(CustomError.message(message: "Unexpected error")))
                return
            }
            
            if let error = requestError {
                completion(.failure(error))
                return
            }
            
            if let responseData = responseData {
                let (data, error): ([Movie]?, Error?) = self.decodeResponse(responseData: responseData)
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                if let data = data {
                    completion(.success(data))
                }
                
            } else {
                completion(.failure(CustomError.message(message: "Invalid data")))
            }
            
        }
    }
    
    private func decodeResponse(responseData: Data) -> ([Movie]?, Error?) {
        do {
            let data = try JSONDecoder().decode(ProviderResponse.self, from: responseData)
            return (data.results, nil)
        } catch {
            return (nil, error)
        }
    }
}

struct ProviderResponse: Codable {
    let page: Int
    let totalResults: Int
    let totalPages: Int
    let results: [Movie]
    
    enum CodingKeys: String, CodingKey {
        case page = "page"
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results = "results"
    }
}

protocol NetworkDataSource {
    func get(url: URL, session: URLSession, completion: @escaping (Data?, Error?) -> Void)
    func get(request: URLRequest, session: URLSession, completion: @escaping (Data?, Error?) -> Void)
}

extension NetworkDataSource {
    
    func get(url: URL, session: URLSession, completion: @escaping (Data?, Error?) -> Void) {
        let urlRequest = URLRequest(url: url)
        get(request: urlRequest, session: session, completion: completion)
    }
    
    func get(request: URLRequest, session: URLSession, completion: @escaping
        (Data?, Error?) -> Void) {
        
        let task = session.dataTask(with: request) {responseData, _, requestError in
            completion(responseData, requestError)
        }
        
        task.resume()
    }
}

enum CustomError: LocalizedError {
    case message(message: String)
    
    var errorDescription: String? {
        switch self {
        case .message(let message):
            return message
        }
    }
}
