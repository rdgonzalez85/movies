//
//  GetMoviesTableViewController.swift
//  Movies
//
//  Created by Rodrigo Gonzalez on 09/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import UIKit

class GetMoviesTableViewController: UITableViewController, StoryboardInstantiateable {
    private enum AccessibilityIdentifier {
        static let accessibilityIdentifier = "GetMoviesTableView"
        static let cellAccessibilityIdentifier = "GetMoviesTableViewCell"
    }
        
    var presenter: GetMoviesPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.accessibilityIdentifier = AccessibilityIdentifier.accessibilityIdentifier
        
        presenter?.getMovies { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return presenter?.numberOfSections() ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.numberOfRowsInSection(section: section) ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        if let movie = presenter?.itemForIndexPath(indexPath: indexPath) {
            cell.textLabel?.text = movie.title
        }
        
        cell.accessibilityIdentifier = "\(AccessibilityIdentifier.cellAccessibilityIdentifier)-\(indexPath.row)"

        return cell
    }
    
}
