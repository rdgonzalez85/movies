//
//  GetMoviesPresenter.swift
//  Movies
//
//  Created by Rodrigo Gonzalez on 09/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import Foundation

protocol TableViewPresenter {
    func numberOfSections() -> Int
    func numberOfRowsInSection(section: Int) -> Int
}

protocol GetMoviesPresenterProtocol: TableViewPresenter {
    func getMovies(completion: @escaping (()->Void) )
    func itemForIndexPath(indexPath: IndexPath) -> UIMovie?
}

class GetMoviesPresenter: GetMoviesPresenterProtocol {

    private let dataSource: MoviesDataSource
    private var movies: [Movie] = []
    
    init(dataSource: MoviesDataSource) {
        self.dataSource = dataSource
    }
 
    func getMovies(completion: @escaping (()->Void) ) {
        dataSource.getMovies { [weak self] (result) in
            guard let self = self else {
                return
            }
            
            switch result {
                case .success(let movies):
                    self.movies = movies
                    completion()
                case .failure(let error):
                    //TODO: handle error
                    print(error)
            }
        }
    }
    
    func numberOfSections() -> Int {
        return movies.count > 0 ? 1 : 0
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return movies.count
    }
    
    func itemForIndexPath(indexPath: IndexPath) -> UIMovie? {
        guard movies.count > 0 else {
            return nil
        }
        
        let selectedMovie = movies[indexPath.row]
        return UIMovie(title: selectedMovie.title, description: selectedMovie.overview)
    }
}

// Won't use an image to make things simpler
struct UIMovie: Equatable {
    let title: String?
    let description : String?
}
