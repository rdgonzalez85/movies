//
//  MoviesUITests.swift
//  MoviesUITests
//
//  Created by Rodrigo Gonzalez on 09/02/2020.
//  Copyright © 2020 Rodrigo Gonzalez. All rights reserved.
//

import XCTest

class MoviesUITests: XCTestCase {

    let app = XCUIApplication()
    
    private enum AccessibilityIdentifier {
        static let accessibilityIdentifier = "GetMoviesTableView"
        static let cellAccessibilityIdentifier = "GetMoviesTableViewCell"
    }
    
    override func setUp() {
        continueAfterFailure = false
        app.launch()
    }

    func testLoadMovieList() {
        
        // get the table
        let table = app.tables.matching(identifier: AccessibilityIdentifier.accessibilityIdentifier)
        
        // get the first item and wait for it to be loaded
        let firstCell = table.cells.element(matching: .cell, identifier: "\(AccessibilityIdentifier.cellAccessibilityIdentifier)-0")
        let loaded = firstCell.waitForExistence(timeout: 3.0)
        
        XCTAssertTrue(loaded)
        
        firstCell.tap()

    }

}
