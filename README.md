## Movies

### What is this repository for? ###

* Example of a simple application using [The Movie DB API](https://www.themoviedb.org/documentation/api)


### How do I get set up? ###

* This project does not use any 3rd party library, so just clone the repo and open `Movies.xcodeproj`
* You'll need an API key from [The Movie DB API](https://www.themoviedb.org/faq/api) and set the corresponding method on `AppDelegate.swift`
* There are 2 targets for tests (`MoviesTests` and `MoviesUITests`)

### Architecture

Given that the app itself is not highly complex the app uses an MVP(with router) approach instead of VIPER. Storyboards are used for quick prototyping but not for classes instantiation neither navigation.
All the classes are backed by a protocol to abstract its instantiation and allow easy switch over new/different solutions and mocks/fakes on unit tests.

### Considerations
Most of the items in the list reflect the decision that was made in order to make the code simple, avoiding the features that wouldn't add much value to the code quality.

* Of the 2 features requested: only the list was implemented since it shares many similarities to the additional information of the movie. It was more interesting to develop unit tests and a clean separation of concerns with the time.
* UI was kept to the minimum to focus on the retrieval of the information - this means super easy cell display and no loading indicators.
* No errors are notified to the user, those are just handled internally - but nothing is done after those reach to the presenter layer.
* No extra work on handling strings is done - e.g. The URL for the API is hardcoded on the App, the correct solution would be to extract this and make it configurable. Once the string is extracted, it will be easy to replace the one that is in the app with the correct one.
* Due to the short time available for the app no cache mechanism was implemented.
